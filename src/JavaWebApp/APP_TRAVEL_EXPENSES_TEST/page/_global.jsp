﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="jp.co.canon_soft.wp.runtime.AppContext"%>
<html>
<head>
<title>Custom header footer Sample</title>
</head>
<body>
<!-- TITLE { -->
  [<%=AppContext.getIoName()%> (<%=AppContext.getIoCode()%>)]
<!-- } TITLE -->

<!-- HEADER { -->
<link href="<%=AppContext.getContextPath()%>/style/customHF.css" rel="stylesheet" type="text/css">

<script language="JavaScript">
<!--
function confirmLogout(){
  myRet = confirm("ログアウトします。よろしいですか？");
  if ( myRet == true ) {
		location.href="<%=AppContext.getContextPath()%>/page/login.jsp";
	}
}
// -->
</script>

<div id="page_header">
<table><tr>
<td id="header_title"><span id="title_text">【旅費精算書】<%=AppContext.getIoName()%></span></td>

<td id="header_info" align="right" valign="bottom">
  <span id="user_info" title="">日付：<%= AppContext.getToday() %>　従業員No：【<%= AppContext.getUser() %>】</span>
 <span id="home"><a href="<%=AppContext.getContextPath()%>/_link.do?i=IO_10001"><img src="<%=AppContext.getContextPath()%>/image/home_16.png" title="ホーム"></a></span>

  <span id="logout"><a style="" onclick="confirmLogout()"><img src="<%=AppContext.getContextPath()%>/image/power_16.png" title="ログアウト"></a></span>
</td>
</tr></table>
</div>
<!-- } HEADER -->

</body>
</html>
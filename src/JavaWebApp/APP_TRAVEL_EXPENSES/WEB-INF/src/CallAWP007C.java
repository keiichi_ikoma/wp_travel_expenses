package aspgm;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.math.BigDecimal;
import jp.co.canon_soft.wp.runtime.AppRuntimeException;
import jp.co.canon_soft.wp.runtime.bp.BpContext;
import jp.co.canon_soft.wp.runtime.bp.BusinessProcess;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;

//RPGからの戻り値をビジネスプロセスに戻すためのクラスimport宣言
//wpappはWebPerformer環境設定「ターゲットパッケージ」に依存する
//詳細は定義ガイド「ビジネスプロセスで拡張ビジネスプロセスを呼び出すには」を参照
import wpapp.dm.Dm_BeanFactory;
import wpapp.dm.DmAWP007C;

public class CallAWP007C implements BusinessProcess {

	// ログ
	private static Log log = LogFactory.getLog(CallAWP007C.class);
	// AS400
	AS400 sys = null;

	public List execute(BpContext context) {
		//ビジネスプロセスからの引数設定
		//Null値をRPGに渡すとエラーが発生する可能性があるため、引数がNull値の場合は代替値に変換する
		String argData1 = "";
		if (context.getArg("@1") != null) argData1 = (String)context.getArg("@1");
		
		return rpgDo(argData1);
	}

	public List rpgDo(String argData1) {
		if (log.isDebugEnabled()) log.debug("RPGプログラム開始");

		// 戻り値用のListを用意します。
		List results = new ArrayList();

		try {
			//AS400オブジェクト作成
			//AS400クラスの引数："サーバ名 or ＩＰアドレス","ユーザーID","パスワード"
			//sys = new AS400("AS400_SERVER","AS400_USER","AS400_PASSWD");
			sys = new AS400("SV05","OPE","TOBAN");


			//実行プログラムオブジェクト作成
			ProgramCall pgm = new ProgramCall(sys);

			//プログラムパス作成
			//QSYSObjectPathNameクラスの引数："スキーマ(ライブラリ名)","プログラムコード","オブジェクトタイプ(RPGの場合は「PGM」)"
			//QSYSObjectPathName pgmPathName = new QSYSObjectPathName("AS400_SCHEMA","RPG01","PGM");
			QSYSObjectPathName pgmPathName = new QSYSObjectPathName("NSOBJJ","AWP007C","PGM");
			//QSYSObjectPathName pgmPathName = new QSYSObjectPathName("NSOJMYJT","AWP007C","PGM");


			//RPGの引数設定
			//下記設定例 第1引数：IN用文字列
			
			//第1引数(IN用)：String型をAS400用に変換
			AS400Text as400Text1 = new AS400Text(10, sys);
			byte[] inData1 = as400Text1.toBytes(argData1);

			//プログラムパラメータ作成
			//配列数はRPGプログラム引数の数
			//ProgramParameter[] paramList = new ProgramParameter[4];
			ProgramParameter[] paramList = new ProgramParameter[1];
			paramList[0] = new ProgramParameter(inData1);
			
			//実行プログラムオブジェクトにプログラムパスとパラメータを設定
			pgm.setProgram(pgmPathName.getPath(), paramList);
			//pgm.setProgram(pgmPathName.getPath());
			//RPGプログラムの実行
			boolean rpgResult = pgm.run();

			if (!rpgResult) {
				//失敗
				AS400Message[] messageList = pgm.getMessageList();
				if (log.isDebugEnabled()) log.debug("実行ＮＧ:" + messageList.toString());

				//ビジネスプロセスに値を戻すための設定
				//詳細は定義ガイド「ビジネスプロセスで拡張ビジネスプロセスを呼び出すには」を参照
				DmAWP007C dmAWP007C = Dm_BeanFactory.createAWP007C();
				dmAWP007C.setWK01("処理失敗：" + messageList.toString());
				//dmRPGWK.setWK02("");
				//dmRPGWK.setWK03(new BigDecimal(0));
				results.add(dmAWP007C);
			} else {
				//成功
				if (log.isDebugEnabled()) log.debug("実行ＯＫ:");

				//RPG戻り値の取得
				//byte[] outData3 = paramList[2].getOutputData();
				//byte[] outData4 = paramList[3].getOutputData();
				//戻り値をString型に変換
				//String rtn3 = (String)as400Text3.toObject(outData3);
				//戻り値をBigDecimal型に変換
				//BigDecimal rtn4 = (BigDecimal)as400PackedDecimal4.toObject(outData4);

				//ビジネスプロセスに値を戻すための設定
				//詳細は定義ガイド「ビジネスプロセスで拡張ビジネスプロセスを呼び出すには」を参照
				DmAWP007C dmAWP007C = Dm_BeanFactory.createAWP007C();
				dmAWP007C.setWK01("処理成功");
				//dmRPGWK.setWK02(rtn3);
				//dmRPGWK.setWK03(rtn4);
				results.add(dmAWP007C);
			}
		} catch (Exception e) {
			throw new AppRuntimeException("ErrorMessage", e);
		} finally {
			sys.disconnectService(AS400.COMMAND);
		}

		if (log.isDebugEnabled()) log.debug("RPGプログラム終了");
		return results;
	}
}